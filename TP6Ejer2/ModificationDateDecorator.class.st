Class {
	#name : #ModificationDateDecorator,
	#superclass : #Decorator,
	#category : #TP6Ejer2
}

{ #category : #'menu commands' }
ModificationDateDecorator >> prettyPrint [
	^ self file prettyPrint , 'Modification date: '
		, self file modificationDate asString , String cr
]

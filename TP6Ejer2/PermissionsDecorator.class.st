Class {
	#name : #PermissionsDecorator,
	#superclass : #Decorator,
	#category : #TP6Ejer2
}

{ #category : #'menu commands' }
PermissionsDecorator >> prettyPrint [
	^ self file prettyPrint , 'Permissions: ' , self file permissions
		, String cr
]

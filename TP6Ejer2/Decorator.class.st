Class {
	#name : #Decorator,
	#superclass : #FileOO,
	#instVars : [
		'file'
	],
	#category : #TP6Ejer2
}

{ #category : #initialization }
Decorator class >> initializeWith: aFile [
	^ self new initializeWith: aFile
]

{ #category : #accessing }
Decorator >> creationDate [
	^ self file creationDate 
	
]

{ #category : #accessing }
Decorator >> extension [
	^ self file extension
	
]

{ #category : #accessing }
Decorator >> file [ 
	^ file
]

{ #category : #initialization }
Decorator >> initializeWith: aFile [
	file := aFile
]

{ #category : #accessing }
Decorator >> modificationDate [
	^ self file modificationDate 
	
]

{ #category : #accessing }
Decorator >> name [
	^ self file name
]

{ #category : #accessing }
Decorator >> permissions [
	^ self file permissions 
]

{ #category : #accessing }
Decorator >> size [
	^ self file size
]

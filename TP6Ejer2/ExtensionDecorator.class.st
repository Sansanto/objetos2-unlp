Class {
	#name : #ExtensionDecorator,
	#superclass : #Decorator,
	#category : #TP6Ejer2
}

{ #category : #'menu commands' }
ExtensionDecorator >> prettyPrint [
	^ self file prettyPrint , 'Extension: ' , self file extension
		, String cr
]

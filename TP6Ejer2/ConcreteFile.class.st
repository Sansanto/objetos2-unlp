Class {
	#name : #ConcreteFile,
	#superclass : #FileOO,
	#instVars : [
		'name',
		'size',
		'extension',
		'creationDate',
		'modificationDate',
		'permissions'
	],
	#category : #TP6Ejer2
}

{ #category : #'as yet unclassified' }
ConcreteFile class >> name: aStringN extension: aStringE size: aFloat [
	^ self new name: aStringN extension: aStringE size: aFloat
]

{ #category : #accessing }
ConcreteFile >> creationDate [
	^ creationDate
	
]

{ #category : #accessing }
ConcreteFile >> creationDate: aDate [
	creationDate := aDate
	
]

{ #category : #accessing }
ConcreteFile >> extension [
	^ extension 
	
]

{ #category : #accessing }
ConcreteFile >> extension: aString [
	extension := aString
	
]

{ #category : #accessing }
ConcreteFile >> modificationDate: aDate [
	modificationDate := aDate
	
]

{ #category : #accessing }
ConcreteFile >> name [
	^ name 
]

{ #category : #accessing }
ConcreteFile >> name: aName [
	name := aName 
]

{ #category : #'as yet unclassified' }
ConcreteFile >> name: aStringN extension: aStringE size: aFloat [
	name := aStringN.
	extension:= aStringE.
	size := aFloat.
	creationDate := Date today.
	modificationDate := Date today.
	permissions := 'all'.
]

{ #category : #accessing }
ConcreteFile >> permissions [
	^ permissions 
]

{ #category : #accessing }
ConcreteFile >> permissions: aString [
	permissions := aString
]

{ #category : #accessing }
ConcreteFile >> size [
	^ size 
	
]

{ #category : #accessing }
ConcreteFile >> size: aReal [
	size := aReal
	
]

Class {
	#name : #CreationDateDecorator,
	#superclass : #Decorator,
	#category : #TP6Ejer2
}

{ #category : #'menu commands' }
CreationDateDecorator >> prettyPrint [
	^ self file prettyPrint , 'Creation date: '
		, self file creationDate asString , String cr
]

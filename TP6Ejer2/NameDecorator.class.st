Class {
	#name : #NameDecorator,
	#superclass : #Decorator,
	#category : #TP6Ejer2
}

{ #category : #'as yet unclassified' }
NameDecorator >> prettyPrint [
	^self file prettyPrint, 'Name: ', self file name, String cr.
]

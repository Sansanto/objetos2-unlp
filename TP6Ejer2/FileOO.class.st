"
Playground:
f1 := ConcreteFile name: 'Archivo' extension: 'txt' size:50.
fn := NameDecorator initializeWith: f1.
fe := ExtensionDecorator initializeWith: fn.
fe prettyPrint.
fc := CreationDateDecorator initializeWith: fe.
fc prettyPrint 
fp := PermissionsDecorator initializeWith: f1.
fn := NameDecorator initializeWith: fp.
fe := ExtensionDecorator initializeWith: fn.
fe prettyPrint.
fc := CreationDateDecorator initializeWith: fe.
fs := SizeDecorator initializeWith: fc.
fs prettyPrint 
"
Class {
	#name : #FileOO,
	#superclass : #Object,
	#category : #TP6Ejer2
}

{ #category : #accessing }
FileOO >> creationDate [ 
	self subclassResponsibility 
]

{ #category : #accessing }
FileOO >> extension [
	self subclassResponsibility 
]

{ #category : #accessing }
FileOO >> modificationDate [ 
	self subclassResponsibility 
]

{ #category : #accessing }
FileOO >> name [
	self subclassResponsibility 
]

{ #category : #accessing }
FileOO >> permissions [
	self subclassResponsibility 
]

{ #category : #'menu commands' }
FileOO >> prettyPrint [ 
	^'Properties: ', String cr.
]

{ #category : #accessing }
FileOO >> size [
	self subclassResponsibility 
]

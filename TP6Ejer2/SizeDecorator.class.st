Class {
	#name : #SizeDecorator,
	#superclass : #Decorator,
	#category : #TP6Ejer2
}

{ #category : #'menu commands' }
SizeDecorator >> prettyPrint [
	^ self file prettyPrint , 'Size: ' , self file size asString
		, String cr
]

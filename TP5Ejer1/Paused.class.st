Class {
	#name : #Paused,
	#superclass : #State,
	#category : #TP5Ejer1
}

{ #category : #initialization }
Paused >> finish [
	^ Finished new
]

{ #category : #'as yet unclassified' }
Paused >> togglePause [
	^ InProgres new
]

"
""Para probar en el playwraund
|item|
item:=ToDoItem name:'Javier'.
item addComent: 'Javier es platinumChariot'.
item addComent: 'Javier es Pucci Yoyikage'.
item start.
item workedTime.
item togglePause .
item workedTime.
item togglePause.
item workedTime.
item finish.
item workedTime.""
"
Class {
	#name : #ToDoItem,
	#superclass : #Object,
	#instVars : [
		'name',
		'state',
		'coments',
		'starTime',
		'endTime'
	],
	#category : #TP5Ejer1
}

{ #category : #accessing }
ToDoItem class >> name: aName [
	^ self new name: aName
]

{ #category : #adding }
ToDoItem >> addComent: aComent [
	state addComent: aComent to: coments
]

{ #category : #'as yet unclassified' }
ToDoItem >> alert [
	state := state alert
]

{ #category : #'as yet unclassified' }
ToDoItem >> canceled [
	state := state canceled
]

{ #category : #accessing }
ToDoItem >> finish [
	state := state finish.
	self setearEndTime
]

{ #category : #initialization }
ToDoItem >> initialize [
	name := 'NombrePorDefecto'.
	coments := OrderedCollection new.
	state := Pending new.
	starTime := nil.
	endTime := nil
]

{ #category : #accessing }
ToDoItem >> name: aName [
	name := aName
]

{ #category : #'as yet unclassified' }
ToDoItem >> relax [
	state := state relax
]

{ #category : #'as yet unclassified' }
ToDoItem >> setearEndTime [
	endTime ifNil: [ endTime := DateAndTime now ]
]

{ #category : #'as yet unclassified' }
ToDoItem >> setearStartTime [
	starTime ifNil: [ starTime := DateAndTime now ]
]

{ #category : #accessing }
ToDoItem >> start [
	state := state start.
	self setearStartTime
]

{ #category : #'as yet unclassified' }
ToDoItem >> togglePause [
	state := state togglePause
]

{ #category : #'as yet unclassified' }
ToDoItem >> workedTime [
	^ state workedTime: starTime to: endTime
]

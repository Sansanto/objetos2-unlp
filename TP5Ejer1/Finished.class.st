Class {
	#name : #Finished,
	#superclass : #State,
	#category : #TP5Ejer1
}

{ #category : #adding }
Finished >> addComent: aComent to: coments [
	^ self
]

{ #category : #'as yet unclassified' }
Finished >> workedTime: aTime1 to: aTime2 [
	^ aTime2 - aTime1
]

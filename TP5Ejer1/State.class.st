Class {
	#name : #State,
	#superclass : #Object,
	#category : #TP5Ejer1
}

{ #category : #adding }
State >> addComent: aComent to: coments [
	coments add: aComent
]

{ #category : #'as yet unclassified' }
State >> alert [
	^ self
]

{ #category : #'as yet unclassified' }
State >> canceled [
	^ Canceled new
]

{ #category : #initialization }
State >> finish [
	^ self
]

{ #category : #'as yet unclassified' }
State >> relax [
	^ self
]

{ #category : #accessing }
State >> start [
	^ self
]

{ #category : #'as yet unclassified' }
State >> togglePause [
	self
		error: 'Debe estar en inProgres o en Paused el estado para pausarlo'
]

{ #category : #'as yet unclassified' }
State >> workedTime: aTime1 to: aTime2 [
	^ DateAndTime now - aTime1
]

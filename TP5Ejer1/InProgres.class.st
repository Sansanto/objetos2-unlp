Class {
	#name : #InProgres,
	#superclass : #State,
	#category : #TP5Ejer1
}

{ #category : #'as yet unclassified' }
InProgres >> alert [
	^ Alert new
]

{ #category : #initialization }
InProgres >> finish [
	^ Finished new
]

{ #category : #'as yet unclassified' }
InProgres >> togglePause [
	^ Paused new
]

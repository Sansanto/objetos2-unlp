Class {
	#name : #Pending,
	#superclass : #State,
	#category : #TP5Ejer1
}

{ #category : #accessing }
Pending >> start [
	^ InProgres new
]

{ #category : #'as yet unclassified' }
Pending >> workedTime: aTime to: aTime2 [
	self error: 'La tarea aun no inicio'
]

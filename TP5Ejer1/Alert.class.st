Class {
	#name : #Alert,
	#superclass : #State,
	#category : #TP5Ejer1
}

{ #category : #'as yet unclassified' }
Alert >> alert [
	^ Canceled new
]

{ #category : #'as yet unclassified' }
Alert >> relax [
	^ InProgres new
]

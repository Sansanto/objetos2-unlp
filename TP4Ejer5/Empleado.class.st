Class {
	#name : #Empleado,
	#superclass : #Object,
	#instVars : [
		'casado',
		'cantHijos'
	],
	#category : #'TP4Ejer5-model'
}

{ #category : #'as yet unclassified' }
Empleado class >> casado: aBoolean hijos: aInteger [
	^ self new casado: aBoolean hijos: aInteger
]

{ #category : #'as yet unclassified' }
Empleado >> adicional [
	^ self extraPorFamilia
]

{ #category : #'as yet unclassified' }
Empleado >> aportesPatronales [
	| total |
	total := self descuento.
	total > 4000
		ifTrue: [ ^ self porcentaje: 3 de: total ].
	total >= 3000
		ifTrue: [ ^ self porcentaje: 2 de: total ].
	^ self porcentaje: 1 de: total
]

{ #category : #'as yet unclassified' }
Empleado >> basico [
	^ self subclassResponsibility
]

{ #category : #'as yet unclassified' }
Empleado >> casado: aBoolean hijos: aInteger [
	casado := aBoolean.
	cantHijos := aInteger
]

{ #category : #'as yet unclassified' }
Empleado >> descuento [
	^ (self porcentaje: 13 de: self basico)
		+ (self porcentaje: 5 de: self adicional)
]

{ #category : #'as yet unclassified' }
Empleado >> extraPorFamilia [
	| total |
	total := 100 * cantHijos.
	casado
		ifTrue: [ total := total + 500 ].
	^ total
]

{ #category : #'as yet unclassified' }
Empleado >> inicialize [
	casado := false.
	cantHijos := 0
]

{ #category : #'as yet unclassified' }
Empleado >> porcentaje: aNum1 de: aNum2 [
	^ aNum2 percent * aNum1
]

{ #category : #'as yet unclassified' }
Empleado >> sueldo [
	^ self basico + self adicional - self descuento
		"- self aportesPatronales"
]

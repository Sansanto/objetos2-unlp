Class {
	#name : #PorContrato,
	#superclass : #Empleado,
	#instVars : [
		'horas'
	],
	#category : #'TP4Ejer5-model'
}

{ #category : #'as yet unclassified' }
PorContrato class >> casado: aBoolean hijos: aCantHijos horas: aInteger [
	^ (self casado: aBoolean hijos: aCantHijos) horas: aInteger
]

{ #category : #'as yet unclassified' }
PorContrato >> adicional [
	horas >= 100
		ifTrue: [ ^ 200 ].
	horas >= 50
		ifTrue: [ ^ 100 ].
	^ 0
]

{ #category : #'as yet unclassified' }
PorContrato >> basico [
	^ horas * 40
]

{ #category : #accessing }
PorContrato >> horas: aInteger [
	horas := aInteger
]

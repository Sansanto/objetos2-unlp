Class {
	#name : #Pasante,
	#superclass : #Empleado,
	#instVars : [
		'cantExamenes'
	],
	#category : #'TP4Ejer5-model'
}

{ #category : #'as yet unclassified' }
Pasante class >> casado: aBoolean hijos: aCantHijos cantExamenes: aCantExamenes [
	^ (self casado: aBoolean hijos: aCantHijos)
		cantExamenes: aCantExamenes
]

{ #category : #calculadorDeSueldo }
Pasante >> adicional [
	^ 100 * cantExamenes
]

{ #category : #calculadorDeSueldo }
Pasante >> basico [
	^ 2000
]

{ #category : #accessing }
Pasante >> cantExamenes: aInteger [
	cantExamenes := aInteger
]

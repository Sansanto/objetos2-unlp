Class {
	#name : #DispositivoTest,
	#superclass : #TestCase,
	#instVars : [
		'dispositivo',
		'ringer',
		'display',
		'crc16',
		'crc32',
		'conectionWiFi',
		'conection4G'
	],
	#category : #'TP5Ejer3-test'
}

{ #category : #running }
DispositivoTest >> setUp [
	"Hooks that subclasses may override to define the fixture of test."

	ringer := Ringer new.
	display := DisplayAlt new.
	dispositivo := Dispositivo ringer: ringer display: display.
	crc16 := CRC16_Calculator new.
	crc32 := CRC32_Calculator new.
	dispositivo configurarCRC: crc16.
	conectionWiFi := WiFiConn new.
	dispositivo conectarCon: conectionWiFi.
	conection4G := FourGAdapter new
]

{ #category : #tests }
DispositivoTest >> testSend [
	Transcript clear.
	dispositivo send: 'spedwawon es el mejor jojobro'.
	self
		assert: Transcript contents
		equals: 'se enviaron los datos con crc 57525'
]

{ #category : #'as yet unclassified' }
DispositivoTest >> testconectarCon [
	Transcript clear.
	self assert: dispositivo conection equals: conectionWiFi.
	dispositivo conectarCon: conection4G.
	self assert: dispositivo conection equals: conection4G.
	Transcript clear
]

{ #category : #'as yet unclassified' }
DispositivoTest >> testconfigurarCRC [
	Transcript clear.
	dispositivo configurarCRC: crc16.
	self assert: dispositivo crc equals: crc16.
	dispositivo configurarCRC: crc32.
	self assert: dispositivo crc equals: crc32.
	Transcript clear
]

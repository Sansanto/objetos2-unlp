Class {
	#name : #FourGConnection,
	#superclass : #Object,
	#instVars : [
		'symb'
	],
	#category : #'TP5Ejer3-model'
}

{ #category : #'as yet unclassified' }
FourGConnection >> symb [
	"devuelve la representacion en string para visualizar esta conexion"

	^ '4G'
]

{ #category : #'as yet unclassified' }
FourGConnection >> transmit: aData crc: aCrc [
	Transcript show: 'se enviaron los datos con crc ' , aCrc asString
]

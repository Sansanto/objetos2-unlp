Class {
	#name : #FourGAdapter,
	#superclass : #Connection,
	#instVars : [
		'fourGConnection'
	],
	#category : #'TP5Ejer3-model'
}

{ #category : #initialization }
FourGAdapter >> initialize [
	fourGConnection := FourGConnection new
]

{ #category : #'as yet unclassified' }
FourGAdapter >> pict [
	^ fourGConnection symb
]

{ #category : #'as yet unclassified' }
FourGAdapter >> sendData: aData crc: aCrc [
	fourGConnection transmit: aData crc: aCrc
]

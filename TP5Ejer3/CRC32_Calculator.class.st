Class {
	#name : #'CRC32_Calculator',
	#superclass : #'CRC_Calculator',
	#category : #'TP5Ejer3-model'
}

{ #category : #'as yet unclassified' }
CRC32_Calculator >> crcFor: aData [
	^ CRC crc32FromCollection: aData
]

Class {
	#name : #WiFiConn,
	#superclass : #Connection,
	#instVars : [
		'pict'
	],
	#category : #'TP5Ejer3-model'
}

{ #category : #'as yet unclassified' }
WiFiConn >> pict [
	"devuelve un string que indica el nombre de la conexion del receptor"

	^ 'Wi-Fi'
]

{ #category : #'as yet unclassified' }
WiFiConn >> sendData: aData crc: aCrc [
	Transcript show: 'se enviaron los datos con crc ' , aCrc asString
]

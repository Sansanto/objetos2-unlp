Class {
	#name : #'CRC_Calculator',
	#superclass : #Object,
	#category : #'TP5Ejer3-model'
}

{ #category : #'as yet unclassified' }
CRC_Calculator >> crcFor: aData [
	^ self subclassResponsibility
]

Class {
	#name : #'CRC16_Calculator',
	#superclass : #'CRC_Calculator',
	#category : #'TP5Ejer3-model'
}

{ #category : #'as yet unclassified' }
CRC16_Calculator >> crcFor: aData [
	^ CRC crc16FromCollection: aData
]

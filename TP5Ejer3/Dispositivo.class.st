Class {
	#name : #Dispositivo,
	#superclass : #Object,
	#instVars : [
		'connection',
		'ringer',
		'display',
		'crc_calculator'
	],
	#category : #'TP5Ejer3-model'
}

{ #category : #'as yet unclassified' }
Dispositivo class >> ringer: aRinger display: aDisplay [
	^ self new ringer: aRinger display: aDisplay
]

{ #category : #commands }
Dispositivo >> conectarCon: aConnection [
	connection := aConnection.
	display showBanner: aConnection pict.
	ringer ring
]

{ #category : #'as yet unclassified' }
Dispositivo >> conection [
	^ connection
]

{ #category : #commands }
Dispositivo >> configurarCRC: unCrcCalculator [
	crc_calculator := unCrcCalculator
]

{ #category : #accessing }
Dispositivo >> crc [
	^ crc_calculator
]

{ #category : #'as yet unclassified' }
Dispositivo >> ringer: aRinger display: aDisplay [
	ringer := aRinger.
	display := aDisplay
]

{ #category : #commands }
Dispositivo >> send: datos [
	| crc |
	crc := crc_calculator crcFor: datos.
	connection sendData: datos crc: crc
]

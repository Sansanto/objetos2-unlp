Class {
	#name : #Connection,
	#superclass : #Object,
	#category : #'TP5Ejer3-model'
}

{ #category : #'as yet unclassified' }
Connection >> pict [
	^ self subclassResponsibility
]

{ #category : #'as yet unclassified' }
Connection >> sendData: aData crc: aCrc [
	Transcript show: 'se enviaron los datos con crc ' , aCrc asString
]

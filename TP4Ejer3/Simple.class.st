Class {
	#name : #Simple,
	#superclass : #Topografia,
	#category : #'TP4Ejer3-model'
}

{ #category : #testing }
Simple >> isEquals: aTopography [
	^ aTopography class = self class
]

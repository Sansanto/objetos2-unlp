Class {
	#name : #Water,
	#superclass : #Simple,
	#category : #'TP4Ejer3-model'
}

{ #category : #'as yet unclassified' }
Water >> cantEarth [
	^ 0
]

{ #category : #'as yet unclassified' }
Water >> cantProductive [
	^ 0
]

{ #category : #'as yet unclassified' }
Water >> cantWater [
	^ 1
]

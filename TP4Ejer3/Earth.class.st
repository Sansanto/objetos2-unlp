Class {
	#name : #Earth,
	#superclass : #Simple,
	#category : #'TP4Ejer3-model'
}

{ #category : #'as yet unclassified' }
Earth >> cantEarth [
	^ 1
]

{ #category : #'as yet unclassified' }
Earth >> cantProductive [
	^ 0.9
]

{ #category : #'as yet unclassified' }
Earth >> cantWater [
	^ 0
]

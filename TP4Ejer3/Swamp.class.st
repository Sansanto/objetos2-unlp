Class {
	#name : #Swamp,
	#superclass : #Simple,
	#category : #'TP4Ejer3-model'
}

{ #category : #'as yet unclassified' }
Swamp >> cantEarth [
	^ 0.3
]

{ #category : #'as yet unclassified' }
Swamp >> cantProductive [
	^ 0.5
]

{ #category : #'as yet unclassified' }
Swamp >> cantWater [
	^ 0.7
]

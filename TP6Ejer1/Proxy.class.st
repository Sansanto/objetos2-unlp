Class {
	#name : #Proxy,
	#superclass : #DatabaseAccess,
	#instVars : [
		'isLoged',
		'databaseRealAcces'
	],
	#category : #TP6Ejer1
}

{ #category : #'as yet unclassified' }
Proxy class >> dataBaseRealAcces: aDataBaseRealAcces [
	^ self new dataBaseRealAcces: aDataBaseRealAcces
]

{ #category : #'as yet unclassified' }
Proxy >> dataBaseRealAcces: aDataBaseRealAcces [
	databaseRealAcces := aDataBaseRealAcces
]

{ #category : #'as yet unclassified' }
Proxy >> getSearchResults: queryString [
	self isLoged
		ifTrue: [ ^ databaseRealAcces getSearchResults: queryString ].
	^ ''
]

{ #category : #initialization }
Proxy >> initialize [
	self isLoged: false
]

{ #category : #'as yet unclassified' }
Proxy >> insertNewRow: rowData [
	"Inserts in the database the row data"

	self isLoged
		ifTrue: [ ^ databaseRealAcces insertNewRow: rowData ].
	^ ''
]

{ #category : #accessing }
Proxy >> isLoged [
	^ isLoged
]

{ #category : #accessing }
Proxy >> isLoged: aLog [
	isLoged := aLog
]
